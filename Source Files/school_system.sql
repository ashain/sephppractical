-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 12, 2016 at 07:39 AM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `school_system`
--

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2016_08_10_093044_tbl_students', 1),
('2016_08_10_093140_tbl_classes', 1),
('2016_08_10_093201_tbl_grades', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_classes`
--

CREATE TABLE IF NOT EXISTS `tbl_classes` (
  `cls_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cls_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`cls_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `tbl_classes`
--

INSERT INTO `tbl_classes` (`cls_id`, `cls_name`) VALUES
(1, 'Primary School'),
(2, 'Junior Level '),
(3, 'Senior Level'),
(4, 'Upper Level');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_grades`
--

CREATE TABLE IF NOT EXISTS `tbl_grades` (
  `grd_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `grd_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `grd_cls_id` int(11) NOT NULL,
  PRIMARY KEY (`grd_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=14 ;

--
-- Dumping data for table `tbl_grades`
--

INSERT INTO `tbl_grades` (`grd_id`, `grd_name`, `grd_cls_id`) VALUES
(1, 'Grade 1', 1),
(2, 'Grade 2', 1),
(3, 'Grade 3', 1),
(4, 'Grade 4', 1),
(5, 'Grade 5', 1),
(6, 'Grade 6', 2),
(7, 'Grade 7', 2),
(8, 'Grade 8', 2),
(9, 'Grade 9', 2),
(10, 'Grade 10', 3),
(11, 'Grade 11', 3),
(12, 'Grade 12', 4),
(13, 'Grade 13', 4);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_students`
--

CREATE TABLE IF NOT EXISTS `tbl_students` (
  `st_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `st_fname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `st_lname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `st_age` int(11) NOT NULL,
  `st_address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `st_cls_id` int(10) unsigned NOT NULL,
  `st_grd_id` int(11) NOT NULL,
  PRIMARY KEY (`st_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=11 ;

--
-- Dumping data for table `tbl_students`
--

INSERT INTO `tbl_students` (`st_id`, `st_fname`, `st_lname`, `st_age`, `st_address`, `st_cls_id`, `st_grd_id`) VALUES
(1, 'Nimal', 'Perera', 10, 'Colombo 3', 2, 6),
(2, 'Saman', 'Fernando', 7, 'Panadura', 1, 3),
(3, 'Mary', 'Silva', 15, 'Colombo 4', 3, 11),
(4, 'Hasan', 'Silva', 17, 'Moratuwa', 4, 13),
(5, 'Warun', 'Fernando', 10, 'Kaluthara', 2, 6),
(6, 'Yasol', 'Perera', 7, 'Ratmalana', 1, 3),
(7, 'John', 'Bass', 13, 'Moratuwa', 2, 9),
(8, 'Gamini', 'Kulatilaka', 5, 'Gampaha', 1, 1),
(9, 'Forsa', 'Zuzan', 14, 'Panadura', 3, 10),
(10, 'Harin', 'Tomson', 15, 'Galle', 3, 11);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
