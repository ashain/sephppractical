@include('inc.header')
@include('inc.navmenu')

 <div class="container">
        
    <div class="row">
        <div class="col-md-3">
           @include('inc.sidenavmenu')
        </div>
        
      <div class="col-md-9">
          
        <h2>View Classes Information</h2>
        
        <hr />
        
        <div>

           <div class="msg_info"></div> 
            
            <!-- Table-to-load-the-data Part -->
            <table class="table">
                <thead>
                    <tr>
                        <th>Class Name</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody id="data_list">
                    <tr>
                        <td colspan="2"><br /><center> <div class="loader"></div> <br /></center></td>
                    </tr>
                </tbody>
            </table>
            
        </div>
      
          
      </div>
    </div>
    
    </div>

    <input type="hidden" name="_token" id="token" value="{{{ csrf_token() }}}" />
    
    <meta name="_token" content="{!! csrf_token() !!}" />
    
    @include('inc.footer')

    <script> 
        $(document).ready(function () {
            viewClases()
        }); 
    </script>