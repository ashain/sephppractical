@include('inc.header')
@include('inc.navmenu')

 <div class="container">
        
    <div class="row">
        <div class="col-md-3">
           @include('inc.sidenavmenu')
        </div>
        
      <div class="col-md-9">
          
        <h2>Create Grades</h2>
        
        <hr />
        
        <div class="msg_alert"></div>
        
        <form class="form-horizontal"id="grade-form" method="post">
            
            <div class="form-group">
              <label for="inputEmail3" class="col-sm-2 control-label">Grade Name</label>
              <div class="col-sm-10">
                  <input type="text" class="form-control" name="grd_name" id="cls_name" placeholder="Enter Here...">
              </div>
            </div>
            
            <div class="form-group">
              <label for="inputEmail3" class="col-sm-2 control-label">Class Type</label>
              <div class="col-sm-10">
                  <select class="form-control" id="grd_cls_id" name="grd_cls_id">
                      <option value=""> Loading... </option>
                  </select> 
              </div>
            </div>
            
            <div class="form-group">
              <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-success">Create Grade</button>
              </div>
            </div>
            
            <input type="hidden" name="_token" id="token" value="{{{ csrf_token() }}}" />
            
        </form>
          
      </div>
        
    </div>
    
    </div>
    
    <meta name="_token" content="{!! csrf_token() !!}" />
    
    @include('inc.footer')
    
    <script> 
        $(document).ready(function () {
            loadClases()
        }); 
    </script>

    