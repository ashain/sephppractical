@include('inc.header')
@include('inc.navmenu')

 <div class="container">
        
    <div class="row">
        <div class="col-md-3">
           @include('inc.sidenavmenu')
        </div>
        
      <div class="col-md-9">
          
        <h2>Create Classes</h2>
        
        <hr />
        
        <div class="msg_alert"></div>
        
        <form class="form-horizontal"id="class-form" method="post">
            
            <div class="form-group">
              <label for="inputEmail3" class="col-sm-2 control-label">Class Name</label>
              <div class="col-sm-10">
                  <input type="text" class="form-control" name="cls_name" id="cls_name" placeholder="Enter Here...">
              </div>
            </div>
            
            <div class="form-group">
              <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-success">Create Class</button>
              </div>
            </div>
            
            <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
            
        </form>
          
      </div>
    </div>
    
    </div>
    
    <meta name="_token" content="{!! csrf_token() !!}" />
    
    @include('inc.footer')

