<!DOCTYPE html>
<html lang="en-US">
<head>
    <title>{{ $data['header_title'] }} - Ashain Dalpadado</title>

    <!-- Load Bootstrap CSS -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/bootstrap.min.css')}}">
    
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/custom.css')}}">
    
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/jquery.alerts.css')}}">
    
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/jquery-ui.css')}}">
    
    <script> var http_path = "{{ URL::to('/') }}/"; </script>
    
</head>
<body>