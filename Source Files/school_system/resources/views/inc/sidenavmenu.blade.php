<div class="row">
    <div class="col-md-12">
        <div class="list-group table-of-contents">
            <a class="list-group-item" href="{{ URL::to('/') }}/create-classes">Create Classes</a>
            <a class="list-group-item" href="{{ URL::to('/') }}/create-grades">Create Grades</a>
            <a class="list-group-item" href="{{ URL::to('/') }}/add-students">Add Students</a>
            <hr />
            <a class="list-group-item" href="{{ URL::to('/') }}/view-classes">View Classes</a>
            <a class="list-group-item" href="{{ URL::to('/') }}/view-grades">View Grades</a>
            <a class="list-group-item" href="{{ URL::to('/') }}/view-students">View Students</a>
        </div>
    </div>
</div> 