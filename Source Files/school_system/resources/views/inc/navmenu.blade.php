<div class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <a href="../" class="navbar-brand">Student Management System</a>
        </div>
        <div class="navbar-collapse collapse" id="navbar-main">
         
          <ul class="nav navbar-nav navbar-right">
            <li class="dropdown">
              <a class="dropdown-toggle" data-toggle="dropdown" href="#" id="themes"> Menu <span class="caret"></span></a>
              <ul class="dropdown-menu" aria-labelledby="">   
                <li><a href="{{ URL::to('/') }}/create-classes">Create Classes</a></li>
                <li><a href="{{ URL::to('/') }}/create-grades">Create Grades</a></li>
                <li><a href="{{ URL::to('/') }}/add-students">Add Students</a></li>
                <div class="divider"></div>
                <li><a href="{{ URL::to('/') }}/view-classes">View Classes</a></li>
                <li><a href="{{ URL::to('/') }}/view-grades">View Grades</a></li>
                <li><a href="{{ URL::to('/') }}/view-students">View Students</a></li>
              </ul>
            </li>
          </ul>

        </div>
      </div>
    </div>
    
    <br /><br /><br /><br />