@include('inc.header')
@include('inc.navmenu')

 <div class="container">
        
    <div class="row">
        <div class="col-md-3">
           @include('inc.sidenavmenu')
        </div>
        
      <div class="col-md-9">
          
        <h2>Add New Student</h2>
        
        <hr />
        
        <div class="msg_alert"></div>
        
        <form class="form-horizontal"id="students-form" method="post">
            
            <div class="form-group">
              <label for="inputEmail3" class="col-sm-2 control-label">First Name</label>
              <div class="col-sm-10">
                  <input type="text" class="form-control" name="st_fname" id="st_fname" placeholder="Enter Here...">
              </div>
            </div>
            
            <div class="form-group">
              <label for="inputEmail3" class="col-sm-2 control-label">Last Name</label>
              <div class="col-sm-10">
                  <input type="text" class="form-control" name="st_lname" id="st_lname" placeholder="Enter Here...">
              </div>
            </div>
            
            <div class="form-group">
              <label for="inputEmail3" class="col-sm-2 control-label">Age</label>
              <div class="col-sm-10">
                  <input type="text" class="form-control" name="st_age" id="st_age" placeholder="Enter Here...">
              </div>
            </div>
            
            <div class="form-group">
              <label for="inputEmail3" class="col-sm-2 control-label">Address</label>
              <div class="col-sm-10">
                  <input type="text" class="form-control" name="st_address" id="st_address" placeholder="Enter Here...">
              </div>
            </div>
            
            <div class="form-group">
              <label for="inputEmail3" class="col-sm-2 control-label">Class Type</label>
              <div class="col-sm-10">
                  <select class="form-control" id="st_cls_id" name="st_cls_id" onchange="loadGradesByClases(this.value)">
                      <option value=""> Loading... </option>
                  </select> 
              </div>
            </div>
            
            <div class="form-group grd-cls" style="display: none;">
              <label for="inputEmail3" class="col-sm-2 control-label">Grade</label>
              <div class="col-sm-10">
                  <select class="form-control" id="st_grd_id" name="st_grd_id">
                      <option value=""> Loading... </option>
                  </select> 
              </div>
            </div>
            
            <div class="form-group">
              <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-success">Add New Student</button>
              </div>
            </div>
            
            <input type="hidden" name="_token" id="token" value="{{{ csrf_token() }}}" />
            
        </form>
          
      </div>
        
    </div>
    
    </div>
    
    <meta name="_token" content="{!! csrf_token() !!}" />
    
    @include('inc.footer')
    
    <script> 
        $(document).ready(function () {
            loadClases()
        }); 
    </script>

