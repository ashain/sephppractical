<?php namespace SchoolSystem;

use Illuminate\Database\Eloquent\Model;

class students extends Model {

        protected $table = 'tbl_students';
	protected $fillable = ['st_id', 'st_fname','st_lname','st_age','st_address','st_cls_id'];

}
