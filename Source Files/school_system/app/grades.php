<?php namespace SchoolSystem;

use Illuminate\Database\Eloquent\Model;

class grades extends Model {
    
        protected $table = 'tbl_grades';
	protected $fillable = ['grd_id', 'grd_name'];

}
