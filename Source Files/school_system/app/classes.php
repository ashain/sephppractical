<?php namespace SchoolSystem;

use Illuminate\Database\Eloquent\Model;

class classes extends Model {

        protected $table = 'tbl_classes';
	protected $fillable = ['cls_id', 'cls_name','cls_grd_id'];

}
