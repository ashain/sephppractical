<?php

namespace SchoolSystem\Http\Controllers;

use SchoolSystem\Http\Requests;
use SchoolSystem\Http\Controllers\Controller;
use DB;
use Illuminate\Http\Request;

class AuthController extends Controller {

    public function index() {
        
    }

    // all are API request functions

    public function showClasses() {
        if (csrf_token() == $_REQUEST['_token']) {
            $reslt = DB::table('tbl_classes')
                    ->select('*')
                    ->get();

            if (count($reslt) > 0) {
                $res_arr = array("Code" => 200, "Status" => "Success", "Count" => count($reslt), "Data" => $reslt);
            } else {
                $res_arr = array("Code" => 400, "Status" => "Fail", "Data" => "");
            }
        } else {
            $res_arr = array("Code" => 89, "Status" => "Invalid or expired token");
        }

        return response()->json($res_arr);
    }

    public function showGrades() {

        if (csrf_token() == $_REQUEST['_token']) {
            $reslt = DB::table('tbl_grades')
                    ->leftJoin('tbl_classes', 'tbl_grades.grd_cls_id', '=', 'tbl_classes.cls_id')
                    ->select('*')
                    ->orderBy('tbl_grades.grd_cls_id', 'ASC')
                    ->get();

            if (count($reslt) > 0) {
                $res_arr = array("Code" => 200, "Status" => "Success", "Count" => count($reslt), "Data" => $reslt);
            } else {
                $res_arr = array("Code" => 400, "Status" => "Fail", "Data" => "");
            }
        } else {
            $res_arr = array("Code" => 89, "Status" => "Invalid or expired token");
        }

        return response()->json($res_arr);
    }

    public function showStudents() {

        if (csrf_token() == $_REQUEST['_token']) {
            $reslt = DB::table('tbl_students')
                    ->leftJoin('tbl_classes', 'tbl_students.st_cls_id', '=', 'tbl_classes.cls_id')
                    ->leftJoin('tbl_grades', 'tbl_students.st_grd_id', '=', 'tbl_grades.grd_id')
                    ->select('*')
                    ->orderBy('tbl_students.st_cls_id', 'DESC')
                    ->get();

            if (count($reslt) > 0) {
                $res_arr = array("Code" => 200, "Status" => "Success", "Count" => count($reslt), "Data" => $reslt);
            } else {
                $res_arr = array("Code" => 400, "Status" => "Fail", "Data" => "");
            }
        } else {
            $res_arr = array("Code" => 89, "Status" => "Invalid or expired token");
        }

        return response()->json($res_arr);
    }

    public function addClasses() {
        //echo csrf_token();

        if (csrf_token() == $_REQUEST['_token']) {
            $status = DB::table('tbl_classes')->insert(
                    array('cls_name' => $_REQUEST['cls_name'])
            );

            if ($status) {
                $res_arr = array("Code" => 200, "Status" => "Success");
            } else {
                $res_arr = array("Code" => 400, "Status" => "Fail");
            }
        } else {
            $res_arr = array("Code" => 89, "Status" => "Invalid or expired token");
        }

        return response()->json($res_arr);
    }

    public function addGrades() {
        //echo csrf_token();
        //print_r($_REQUEST); die();

        if (csrf_token() == $_REQUEST['_token']) {
            $status = DB::table('tbl_grades')->insert(
                    array(
                        'grd_name' => $_REQUEST['grd_name'],
                        'grd_cls_id' => $_REQUEST['grd_cls_id']
                    )
            );

            if ($status) {
                $res_arr = array("Code" => 200, "Status" => "Success");
            } else {
                $res_arr = array("Code" => 400, "Status" => "Fail");
            }
        } else {
            $res_arr = array("Code" => 89, "Status" => "Invalid or expired token");
        }

        return response()->json($res_arr);
    }

    public function addStudents() {
        //echo csrf_token();
        //print_r($_REQUEST); die();

        if (csrf_token() == $_REQUEST['_token']) {
            $status = DB::table('tbl_students')->insert(
                    array(
                        'st_fname' => $_REQUEST['st_fname'],
                        'st_lname' => $_REQUEST['st_lname'],
                        'st_age' => $_REQUEST['st_age'],
                        'st_address' => $_REQUEST['st_address'],
                        'st_cls_id' => $_REQUEST['st_cls_id'],
                        'st_grd_id' => $_REQUEST['st_grd_id'],
                    )
            );

            if ($status) {
                $res_arr = array("Code" => 200, "Status" => "Success");
            } else {
                $res_arr = array("Code" => 400, "Status" => "Fail");
            }
        } else {
            $res_arr = array("Code" => 89, "Status" => "Invalid or expired token");
        }

        return response()->json($res_arr);
    }

    public function deleteClasses() {

        if (csrf_token() == $_REQUEST['_token']) {

            $status = DB::table('tbl_classes')->where('cls_id', '=', $_REQUEST['id'])->delete();

            if ($status) {
                $res_arr = array("Code" => 200, "Status" => "Success");
            } else {
                $res_arr = array("Code" => 400, "Status" => "Fail");
            }
        } else {
            $res_arr = array("Code" => 89, "Status" => "Invalid or expired token");
        }

        return response()->json($res_arr);
    }

    public function deleteGrades() {

        if (csrf_token() == $_REQUEST['_token']) {

            $status = DB::table('tbl_grades')->where('grd_id', '=', $_REQUEST['id'])->delete();

            if ($status) {
                $res_arr = array("Code" => 200, "Status" => "Success");
            } else {
                $res_arr = array("Code" => 400, "Status" => "Fail");
            }
        } else {
            $res_arr = array("Code" => 89, "Status" => "Invalid or expired token");
        }

        return response()->json($res_arr);
    }

    public function deleteStudents() {

        if (csrf_token() == $_REQUEST['_token']) {

            $status = DB::table('tbl_students')->where('st_id', '=', $_REQUEST['id'])->delete();

            if ($status) {
                $res_arr = array("Code" => 200, "Status" => "Success");
            } else {
                $res_arr = array("Code" => 400, "Status" => "Fail");
            }
        } else {
            $res_arr = array("Code" => 89, "Status" => "Invalid or expired token");
        }

        return response()->json($res_arr);
    }

    public function loadClasses() {

        if (csrf_token() == $_REQUEST['_token']) {
            $res_arr = DB::table('tbl_classes')
                    ->select('*')
                    ->get();

            //print_r($results);
        } else {
            $res_arr = array("Code" => 89, "Status" => "Invalid or expired token");
        }

        return response()->json($res_arr);
    }

    public function loadGradesByClasses() {

        if (csrf_token() == $_REQUEST['_token']) {
            $res_arr = DB::table('tbl_grades')
                    ->select('*')
                    ->where('grd_cls_id', $_REQUEST['cls_id'])
                    ->get();

            //print_r($results);
        } else {
            $res_arr = array("Code" => 89, "Status" => "Invalid or expired token");
        }

        return response()->json($res_arr);
    }

}
