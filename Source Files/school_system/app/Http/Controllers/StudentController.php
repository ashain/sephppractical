<?php namespace SchoolSystem\Http\Controllers;

use SchoolSystem\Http\Requests;
use SchoolSystem\Http\Controllers\Controller;

use Illuminate\Http\Request;
use illuminate\html;
use DB;

class StudentController extends Controller {

	
	public function index()
	{
                $argument = array("header_title"=>"View Students Information | School Management System");
		return view('view-students')->with('data', $argument);
	}

        // view data
        public function showClasses()
	{
		$argument = array("header_title"=>"View Classes Information | School Management System");
                return view('view-classes')->with('data', $argument); // view classes table
	}
        
        public function showGrades()
	{
		$argument = array("header_title"=>"View Grades Information | School Management System");
                return view('view-grades')->with('data', $argument); // view grades table
	}
        
        public function showStudents()
	{
		$argument = array("header_title"=>"View Students Information | School Management System");
                return view('view-students')->with('data', $argument); // view students table
	}
        
        
        
        // add data forms
        public function addClasses()
	{
                $argument = array("header_title"=>"Create Classes Information | School Management System");
                return view('create-classes')->with('data', $argument); // view class create forms
                
	}
        
        public function addGrades()
	{
                $argument = array("header_title"=>"Create Grades Information | School Management System");
                return view('create-grades')->with('data', $argument); // view grade create forms
                
	}
        
        public function addStudents()
	{
                $argument = array("header_title"=>"Add Students Information | School Management System");
                return view('add-students')->with('data', $argument); // view student create forms
                
	}
	

}
