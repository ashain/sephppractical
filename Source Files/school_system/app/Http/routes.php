<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// LOCAL --------------------------------------

Route::get('/', 'StudentController@index');

Route::get('view-classes', 'StudentController@showClasses');
Route::get('view-grades', 'StudentController@showGrades');
Route::get('view-students', 'StudentController@showStudents');

Route::get('create-classes', 'StudentController@addClasses');
Route::get('create-grades', 'StudentController@addGrades');
Route::get('add-students', 'StudentController@addStudents');

// LOCAL END ---------------------------------


// API CALL---------------------------------------

// view data
Route::post('respond/api/v1/json/view-classes', 'AuthController@showClasses');
Route::get('respond/api/v1/json/view-classes', 'AuthController@showClasses');

Route::post('respond/api/v1/json/view-grades', 'AuthController@showGrades');
Route::get('respond/api/v1/json/view-grades', 'AuthController@showGrades');

Route::post('respond/api/v1/json/view-students', 'AuthController@showStudents');
Route::get('respond/api/v1/json/view-students', 'AuthController@showStudents');

Route::post('respond/api/v1/json/load-classes', 'AuthController@loadClasses');
Route::get('respond/api/v1/json/load-classes', 'AuthController@loadClasses');

Route::post('respond/api/v1/json/load-grades', 'AuthController@loadGradesByClasses');
Route::get('respond/api/v1/json/load-grades', 'AuthController@loadGradesByClasses');


// add data and view
Route::post('respond/api/v1/json/create-classes', 'AuthController@addClasses');
Route::get('respond/api/v1/json/create-classes', 'AuthController@addClasses');

Route::post('respond/api/v1/json/create-grades', 'AuthController@addGrades');
Route::get('respond/api/v1/json/create-grades', 'AuthController@addGrades');

Route::post('respond/api/v1/json/add-students', 'AuthController@addStudents');
Route::get('respond/api/v1/json/add-students', 'AuthController@addStudents');


// delete data and view
Route::post('respond/api/v1/json/delete-class', 'AuthController@deleteClasses');
Route::get('respond/api/v1/json/delete-class', 'AuthController@deleteClasses');

Route::post('respond/api/v1/json/delete-grade', 'AuthController@deleteGrades');
Route::get('respond/api/v1/json/delete-grade', 'AuthController@deleteGrades');

Route::post('respond/api/v1/json/delete-student', 'AuthController@deleteStudents');
Route::get('respond/api/v1/json/delete-student', 'AuthController@deleteStudents');

// API END ------------------------------------

