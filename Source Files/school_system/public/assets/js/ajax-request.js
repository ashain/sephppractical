
$(document).ready(function () {
//by using the validation plugin
    $("#class-form").validate({ // validate classes adding form
        rules:{
            cls_name: "required",
        },
        submitHandler: function (form) {
            addClasses();
        }

    });
    
    $("#grade-form").validate({ // validate grades adding form
        rules:{
            grd_name: "required",
            grd_cls_id: "required",
        },
        submitHandler: function (form) {
            addGrades();
        }

    });
    
    $("#students-form").validate({ // validate students adding form
        rules:{
            st_fname: "required",
            st_lname: "required",
            st_age: {required:true, number:true, maxlength:2},
            st_cls_id: "required",
            st_grd_id: "required",
        },
        submitHandler: function (form) {
            addStudents();
        }

    });
    
});

function addClasses(){  // adding classes data
    
    $.ajax({
      url: http_path+'respond/api/v1/json/create-classes',
      type: "post",
      data: $("#class-form").serialize(),
      success: function(response){
         if(response.Code == 200){
            showAlert("Data Successfully Added", 'S', '');
            $("#class-form").trigger('reset');
         }else{
            showAlert("Opps Some Error Occured", 'F', '');     
         }
      }
    });
    
}


function addGrades(){  // adding grades data
    
    $.ajax({
      url: http_path+'respond/api/v1/json/create-grades',
      type: "post",
      data: $("#grade-form").serialize(),
      success: function(response){
         if(response.Code == 200){
            showAlert("Data Successfully Added", 'S', ''); 
            $("#grade-form").trigger('reset'); 
         }else{
            showAlert("Opps Some Error Occured", 'F', '');     
         }
      }
    });
    
}

function addStudents(){  // adding students data
    
    $.ajax({
      url: http_path+'respond/api/v1/json/add-students',
      type: "post",
      data: $("#students-form").serialize(),
      success: function(response){
         if(response.Code == 200){
            showAlert("Data Successfully Added", 'S', '');
            $("#students-form").trigger('reset');
            $(".grd-cls").hide();
         }else{
            showAlert("Opps Some Error Occured", 'F', '');     
         }
      }
    });
    
}

function loadClases(){ 
    
    var dataUrl = "&_token="+$("#token").val();
    
    $.ajax({
      url: http_path+'respond/api/v1/json/load-classes',
      type: "post",
      data:dataUrl,
      success: function(response){ 
          $('#grd_cls_id, #st_cls_id').html('');
          $('#grd_cls_id, #st_cls_id').append('<option value="">-- Select Class --</option>');   
          $.each(response, function (i, v) {
             $('#grd_cls_id, #st_cls_id').append('<option value="'+response[i].cls_id+'">'+response[i].cls_name+'</option>');
          });
        
      }
    });
    
}

function loadGradesByClases(id){
    
    var dataUrl = "&_token="+$("#token").val()+"&cls_id="+id;
    
    $.ajax({
      url: http_path+'respond/api/v1/json/load-grades',
      type: "post",
      data:dataUrl,
      success: function(response){
          $('.grd-cls').fadeIn();
          $('#st_grd_id').html('');
          $('#st_grd_id').append('<option value="">-- Select Grades --</option>');   
          $.each(response, function (i, v) {
             $('#st_grd_id').append('<option value="'+response[i].grd_id+'">'+response[i].grd_name+'</option>');
          });
        
      }
    });
    
}


function viewClases(){  // view classes data
    
    var dataUrl = "&_token="+$("#token").val();
    
    $.ajax({
      url: http_path+'respond/api/v1/json/view-classes',
      type: "post",
      data:dataUrl,
      success: function(response){ 
          $('#data_list').html("");
          if(response.Code == 200){
            infoAlert(response.Count+' Records Found'); 
            
            if(response.Count > 0){
                $.each(response.Data, function (i, v) {
                   $('#data_list').append(
                        '<tr id="row_'+response.Data[i].cls_id+'">'+
                          '<td>'+response.Data[i].cls_name+'</td>'+
                          '<td><button onclick="deleteClases('+response.Data[i].cls_id+')" class="btn btn-danger btn-xs btn-delete"  value="">Delete</button></td>'+
                        '</tr>'
                   );
                });
            }
            
          }else if(response.Code == 400){
              showAlert('Opps! error in respond', 'F', '');
          }else if(response.Code == 89){
              showAlert('Invalid Token', 'I', '');
          }
      }
    });
    
}

function viewGrades(){  // adding grades data
    
    var dataUrl = "&_token="+$("#token").val();
    
    $.ajax({
      url: http_path+'respond/api/v1/json/view-grades',
      type: "post",
      data:dataUrl,
      success: function(response){ 
          $('#data_list').html("");
          if(response.Code == 200){
            infoAlert(response.Count+' Records Found'); 
            var clna;
            if(response.Count > 0){
                $.each(response.Data, function (i, v) {
                    
                    if(response.Data[i].cls_name == null){
                        clna = '<span style="color:red;">not assigned</span>';
                    }else{
                        clna = response.Data[i].cls_name;
                    }
                    
                   $('#data_list').append(
                        '<tr id="row_'+response.Data[i].grd_id+'">'+
                          '<td>'+response.Data[i].grd_name+'</td>'+
                          '<td>'+clna+'</td>'+
                          '<td><button onclick="deleteGrade('+response.Data[i].grd_id+')" class="btn btn-danger btn-xs btn-delete" value="">Delete</button></td>'+
                        '</tr>'
                   );
                });
            }
            
          }else if(response.Code == 400){
              showAlert('Opps! error in respond', 'F', '');
          }else if(response.Code == 89){
              showAlert('Invalid Token', 'I', '');
          }
      }
    });
    
}

function viewStudents(){  // adding students data
    
    var dataUrl = "&_token="+$("#token").val();
    
    $.ajax({
      url: http_path+'respond/api/v1/json/view-students',
      type: "post",
      data:dataUrl,
      success: function(response){ 
          $('#data_list').html("");
          if(response.Code == 200){
            infoAlert(response.Count+' Records Found'); 
            
            var clna;
            var grna;
            
            if(response.Count > 0){
                $.each(response.Data, function (i, v) {
                    
                    if(response.Data[i].cls_name == null){
                        clna = '<span style="color:red;">not assigned</span>';
                    }else{
                        clna = response.Data[i].cls_name;
                    }
                    
                    if(response.Data[i].grd_name == null){
                        grna = '<span style="color:red;">not assigned</span>'; 
                    }else{
                        grna = response.Data[i].grd_name; 
                    }
                    
                   $('#data_list').append(
                        '<tr id="row_'+response.Data[i].st_id+'">'+
                          '<td>'+response.Data[i].st_fname+'</td>'+
                          '<td>'+response.Data[i].st_lname+'</td>'+
                          '<td>'+response.Data[i].st_age+'</td>'+
                          '<td>'+response.Data[i].st_address+'</td>'+
                          '<td>'+clna+'</td>'+
                          '<td>'+grna+'</td>'+
                          '<td><button onclick="deleteStudent('+response.Data[i].st_id+')" class="btn btn-danger btn-xs btn-delete" value="">Delete</button></td>'+
                        '</tr>'
                   );
                });
            }
            
          }else if(response.Code == 400){
              showAlert('Opps! error in respond', 'F', '');
          }else if(response.Code == 89){
              showAlert('Invalid Token', 'I', '');
          }
      }
    });
    
}

function deleteClases(id){  // delete classes data
    
    jConfirm('Do you want to delete?', 'Confirmation Delete', function(r) {
        if(r){
    
            var dataUrl = "&_token="+$("#token").val()+"&id="+id;

            $.ajax({
              url: http_path+'respond/api/v1/json/delete-class',
              type: "post",
              data:dataUrl,
              success: function(response){    
                  if(response.Code == 200){  
                    $("#row_"+id).fadeOut();
                    setTimeout(function(){viewClases()}, 500);
                  }
              }
            });
        }
    });
}

function deleteGrade(id){  // delete grades data
    
    jConfirm('Do you want to delete?', 'Confirmation Delete', function(r) {
        if(r){
    
            var dataUrl = "&_token="+$("#token").val()+"&id="+id;

            $.ajax({
              url: http_path+'respond/api/v1/json/delete-grade',
              type: "post",
              data:dataUrl,
              success: function(response){    
                  if(response.Code == 200){  
                    $("#row_"+id).fadeOut();
                    setTimeout(function(){viewGrades()}, 500);
                  }
              }
            });
        }
    });
}

function deleteStudent(id){  // adding students data
    
    jConfirm('Do you want to delete?', 'Confirmation Delete', function(r) {
        if(r){
    
            var dataUrl = "&_token="+$("#token").val()+"&id="+id;

            $.ajax({
              url: http_path+'respond/api/v1/json/delete-student',
              type: "post",
              data:dataUrl,
              success: function(response){    
                  if(response.Code == 200){  
                    $("#row_"+id).fadeOut();
                    setTimeout(function(){viewStudents()}, 500);
                  }
              }
            });
        }
    });
}

//------------------------------------------ aditional options ---------------------------------------------------------------

function showAlert(message, type, url) {

    $(".msg_alert").show();

    if (type == "S") {
        $(".msg_alert").html('<div role="alert" class="alert alert-success">' + message + '</div>');
        if(url != '')
        setTimeout(function(){location.href = http_path + url }, 1200);
    } else if (type == "F") {
        $(".msg_alert").html('<div role="alert" class="alert alert-danger">' + message + '</div>');
    } else if (type == "I") {
        $(".msg_alert").html('<div role="alert" class="alert alert-info">' + message + '</div>');
    } else {
        $(".msg_alert").hide();
    }

    setTimeout(function () {
        $(".msg_alert").fadeOut();
    }, 1000);

}

function infoAlert(message) {

    $(".msg_info").show();

    $(".msg_info").html('<div role="alert" class="alert alert-info">' + message + '</div>');
    

}