<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblStudents extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
            Schema::create('tbl_students', function (Blueprint $table) {
                $table->increments('st_id');
                $table->string('st_fname');
                $table->string('st_lname');
                $table->integer('st_age');
                $table->string('st_address');
                $table->integer('st_cls_id');
                $table->integer('st_grd_id');
            });
            
            /*Schema::table('tbl_students', function($table) {
                $table->foreign('st_cls_id')->references('cls_id')->on('tbl_classes');
            });*/
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
            Schema::drop('tbl_students');
	}

}
