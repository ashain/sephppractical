<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblClasses extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
            Schema::create('tbl_classes', function (Blueprint $table) {
                $table->increments('cls_id');
                $table->string('cls_name');
            });
            
            /*Schema::table('tbl_classes', function($table) {
                $table->foreign('cls_grd_id')->references('grd_id')->on('tbl_grades');
            });*/
            
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
            Schema::drop('tbl_classes');
	}

}
